package characters

import java.awt.Image

import objects.GameObject
import utils.{Res, Utils}

/**
  * Created by nicola on 09/04/17.
  */
trait Character {
  val PROXIMITY_MARGIN = 10

  def walk: Image

  def isNearby(character: Character): Boolean

  def isNearby(obj: GameObject): Boolean

  def contact(pers: Character): Unit

  def contact(obj: GameObject): Unit

  def getX: Int

  def getY: Int

  def getWidth: Int

  def getHeight: Int

  def isAlive: Boolean

  def deadImage: Image

  def setMoving(moving: Boolean): Unit

  def setAlive(alive: Boolean): Unit

  def isToRight: Boolean
}

abstract class AbstractCharacter(var x: Int) extends Character{
  var moving = false
  var toRight = true
  var counter  = 0
  var alive = true
  var y = 0

  def setY(y: Int) = this.y = y

  def setX(x: Int) = this.x = x

  override def getX: Int = x

  override def getY: Int = y

  override def walk: Image = Utils.getImage(doWalk())

  protected def doWalk(): String

  override def setMoving(moving: Boolean): Unit = this.moving = moving

  override def setAlive(alive: Boolean): Unit = this.alive = alive

  override def isToRight: Boolean = toRight

  def setToRight(right: Boolean): Unit = this.toRight = right

  override def isAlive: Boolean = alive

  def isNearby(pers: Character): Boolean = {
    if ((this.x > pers.getX - PROXIMITY_MARGIN && this.x < pers.getX + pers.getWidth + PROXIMITY_MARGIN) || (this.x + this.getWidth > pers.getX - PROXIMITY_MARGIN && this.x + this.getWidth < pers.getX + pers.getWidth + PROXIMITY_MARGIN)) return true
    return false
  }

  def isNearby(obj: GameObject): Boolean = { if ((this.x > obj.getGameObjectX - PROXIMITY_MARGIN && this.x < obj.getGameObjectX + obj.getWidth + PROXIMITY_MARGIN) || (this.getX + this.getWidth > obj.getGameObjectX - PROXIMITY_MARGIN && this.x + this.getWidth < obj.getGameObjectX + obj.getWidth + PROXIMITY_MARGIN)) return true
    return false
  }

  def incCounter(): Unit = {this.counter += 1}

  def hitAhead(og: GameObject): Boolean = if (this.x + this.getWidth < og.getGameObjectX || this.x + this.getWidth > og.getGameObjectX + 5 || this.y + this.getHeight <= og.getGameObjectY || this.y >= og.getGameObjectY + og.getHeight) false
  else true


  def hitBack(og: GameObject): Boolean = if (this.x > og.getGameObjectX + og.getWidth || this.x + this.getWidth < og.getGameObjectX + og.getWidth - 5 || this.y + this.getHeight <= og.getGameObjectY || this.y >= og.getGameObjectY + og.getHeight) false
  else true

  def hitBelow(og: GameObject): Boolean = if (this.x + this.getWidth < og.getGameObjectX + 5 || this.x > og.getGameObjectX + og.getWidth - 5 || this.y + this.getHeight < og.getGameObjectY || this.y + this.getHeight > og.getGameObjectY + 5) false
  else true

  def hitAbove(og: GameObject): Boolean = if (this.x + this.getWidth < og.getGameObjectX + 5 || this.x > og.getGameObjectX + og.getWidth - 5 || this.y < og.getGameObjectY + og.getHeight || this.y > og.getGameObjectY + og.getHeight + 5) false
  else true

  def hitAhead(pers: Character): Boolean =
    if (this.isToRight)
      if (this.x + this.getWidth < pers.getX || this.x + this.getWidth > pers.getX + 5 || this.y + this.getHeight <= pers.getY || this.y >= pers.getY + pers.getHeight) false
      else true
    else false


  def hitBack(pers: Character): Boolean = if (this.x > pers.getX + pers.getWidth || this.x + this.getWidth < pers.getX + pers.getWidth - 5 || this.y + this.getHeight <= pers.getY || this.y >= pers.getY + pers.getHeight) false
  else true


  def hitBelow(pers: Character): Boolean = if (this.x + this.getWidth < pers.getX || this.x > pers.getX + pers.getWidth || this.y + this.getHeight < pers.getY || this.y + this.getHeight > pers.getY) false
  else true

}
