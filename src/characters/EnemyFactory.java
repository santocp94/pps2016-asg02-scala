package characters;

/**
 * Created by nicola on 15/03/17.
 */
public interface EnemyFactory {
    AbstractEnemy createMushroom(int x);

    AbstractEnemy createTurtle(int x);

    AbstractEnemy createTBoo(int x);
}
