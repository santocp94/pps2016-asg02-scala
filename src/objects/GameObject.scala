package objects

import java.awt.Image

import game.Main
import utils.{Res, Utils}

/**
  * Created by nicola on 09/04/17.
  */


trait GameObject {

  def getWidth: Int

  def getHeight: Int

  def getGameObjectY: Int

  def getGameObjectX: Int

  def move(): Unit

  def getImgObj: Image

}

abstract class AbstractGameObject(var x: Int, y: Int) extends GameObject{
  override def getGameObjectX: Int = x

  override def getGameObjectY: Int = y

  override def move(): Unit = { if(Main.scene.getxPos() >= 0) this.x = this.x - Main.scene.getMov }
}

object Block {

  def apply(x: Int, y: Int): Block = new Block(x, y)

  class Block(x: Int, y: Int) extends AbstractGameObject(x, y){
    override def getWidth: Int = 30

    override def getHeight: Int = 30

    override def getImgObj: Image = Utils.getImage(Res.IMG_BLOCK)
  }
}

object Tunnel {

  def apply(x: Int, y: Int): Tunnel = new Tunnel(x, y)

  class Tunnel(x: Int, y: Int) extends AbstractGameObject(x, y){
    override def getWidth: Int = 43

    override def getHeight: Int = 65

    override def getImgObj: Image = Utils.getImage(Res.IMG_TUNNEL)
  }
}

object Coin {

  def apply(x: Int, y: Int): Coin = new Coin(x, y)

  class Coin(x: Int, y: Int)  extends AbstractGameObject(x, y){
    override def getWidth: Int = 30

    override def getHeight: Int = 30

    override def getImgObj: Image = Utils.getImage(Res.IMG_PIECE1)
  }
}

