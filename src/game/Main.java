package game;

import javax.swing.*;

public class Main {

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;
    private static final String WINDOW_TITLE = "Super Mario";
    public static Platform scene;

    public static void main(String[] args) {
        JFrame gameWindow = new JFrame(WINDOW_TITLE);
        gameWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gameWindow.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        gameWindow.setLocationRelativeTo(null);
        gameWindow.setResizable(true);
        gameWindow.setAlwaysOnTop(true);

        scene = new Platform();
        gameWindow.setContentPane(scene);
        gameWindow.setVisible(true);

        Thread timer = new Thread(new Refresh());
        timer.start();
    }

}
