package game

import objects.{Block, Coin, GameObject, Tunnel}

/**
  * Created by nicola on 09/04/17.
  */

class MapGenerator {

  import scala.collection.mutable.ListBuffer
  import collection.JavaConverters._

  def generateBlocksAndTunnels: java.util.List[GameObject] = {

    val objects = ListBuffer[GameObject]()

    //with more time, should read this from a file
    objects += Tunnel(500, 230)
    objects += Tunnel(1000, 230)
    objects += Tunnel(1600, 230)
    objects += Tunnel(1900, 230)
    objects += Tunnel(2500, 230)
    objects += Tunnel(3000, 230)
    objects += Tunnel(3800, 230)
    objects += Tunnel(4500, 230)
    objects += Block(400, 180)
    objects += Block(1200, 180)
    objects += Block(1270, 170)
    objects += Block(1340, 160)
    objects += Block(2000, 180)
    objects += Block(2600, 160)
    objects += Block(2650, 180)
    objects += Block(3500, 160)
    objects += Block(3550, 140)
    objects += Block(4000, 170)
    objects += Block(4200, 200)
    objects += Block(4300, 210)

    val outObjects: java.util.List[GameObject] = objects.asJava
    outObjects
  }

  def generateCoins: java.util.List[GameObject] = {
    val coins = ListBuffer[GameObject]()

    coins += Coin(402, 145)
    coins += Coin(1202, 140)
    coins += Coin(1272, 95)
    coins += Coin(1342, 40)
    coins += Coin(1650, 145)
    coins += Coin(2650, 145)
    coins += Coin(3000, 145)
    coins += Coin(3400, 135)
    coins += Coin(4200, 145)
    coins += Coin(4600, 40)

    val outCoins: java.util.List[GameObject] = coins.asJava
    outCoins
  }
}