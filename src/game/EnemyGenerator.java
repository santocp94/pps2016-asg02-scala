package game;

//import characters.AbstractEnemy;
import characters.AbstractEnemy;
import characters.Character;
import characters.EnemyFactory;
import characters.EnemyFactoryImpl;

import java.util.Random;

/**
 * Created by nicola on 16/03/17.
 */
public class EnemyGenerator {

    private static final int MAX_ENEMY_POSITION = 3500;
    private static final int ENEMY_POSITION_OFFSET = 800;

    private EnemyFactory enemyFactory;
    private Random random = new Random();

    public EnemyGenerator(){
        this.enemyFactory = new EnemyFactoryImpl();
    }

    //now unused, but could be usefull
    public Character generateTurtle(){
        return enemyFactory.createTurtle(random.nextInt(MAX_ENEMY_POSITION) + ENEMY_POSITION_OFFSET);
    }

    public Character generateMushroom(){
        return enemyFactory.createMushroom(random.nextInt(MAX_ENEMY_POSITION) + ENEMY_POSITION_OFFSET);
    }

    public AbstractEnemy generateRandomEnemy(){
        int spawnPosition = 0;
        while(spawnPosition < 550) spawnPosition = random.nextInt(4000);
        double r = random.nextDouble();
        if(r < 0.33) return enemyFactory.createMushroom(spawnPosition);
        if(r > 0.66) return enemyFactory.createTurtle(spawnPosition);
        return enemyFactory.createTBoo(spawnPosition);
    }

}
