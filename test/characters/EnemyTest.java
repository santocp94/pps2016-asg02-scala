package characters;

import game.EnemyGenerator;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by nicola on 16/03/17.
 */
public class EnemyTest {
    private static final int ITERATION = 1000;
    EnemyGenerator enemyGenerator =  new EnemyGenerator();

    @Test
    public void testEnemyGeneration() throws Exception {
        for(int i = 0; i < ITERATION; i++) {
            Character enemy = enemyGenerator.generateRandomEnemy();
            assertTrue(enemy.isAlive());
            assertTrue(enemy.getX() >= 0 && enemy.getX() <= 4000);
        }
    }
}